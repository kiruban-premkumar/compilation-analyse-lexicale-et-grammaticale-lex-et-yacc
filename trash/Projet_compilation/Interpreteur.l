%option noyywrap
%%
"#".*\n                     ;
"func"                      {return FUNC;}
"print"                     {return PRINT;}
"eval"                     	{return EVAL;}
"simplify"                  {return SIMPLIFY;}
"plot"                     	{return PLOT;}
(-)?[0-9]*(\.)?[0-9]+       {yylval.val = atof(yytext); return REEL;}
"/"[eap]                    {yylval.cc = yytext[1]; return FRM;}
[a-zA-Z][a-zA-Z0-9_]+[']*   {yylval.str = strdup(yytext); return NAMEFUNC;}
\"[^"\n]*\"                 {yylval.str = strdup(yytext); return FILENAME;}//?
":"                         {return DP;}
"+"                         {return PLUS;}
"*"                         {return FOIS;}
"-"							{return MOINS;}
"("                         {return PARENTHESE_G;}
")"                         {return PARENTHESE_D;}
"x"							{return XV;}
[ \t]                     ;
\n							{return EOL;}
.                           {return ERROR;}
%%

int yyerror(char * mess)
{
    fprintf(stderr, "%s near %s\n", mess, yytext);
	exit(0);
}

