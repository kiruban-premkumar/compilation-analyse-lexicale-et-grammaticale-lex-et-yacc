%{	
    #include <stdlib.h>
    #include <stdio.h>
    #include <math.h>
	#include "Noeud.h"
    	
	char buffer[BUFFER_MAX_SIZE];
	static FILE * plot = NULL;
	
	// FLAGS
	static int firstFname = 1; // 1 : premiere fonction ajoutee a plot, 0 sinon

%}


%union{
float val;
char* str;
char cc;
Noeud* noeud;//TPA_Expr * exp;
};



/*commandes*/
%token FUNC PRINT DP SIMPLIFY PLOT EVAL XV EOL
%token <val> REEL

/*format, parentheses, variables*/
%token <cc> FRM PARENTHESE_G PARENTHESE_D

/*operateurs*/
%token PLUS FOIS MOINS
%left PLUS
%left FOIS
%left MOINS
%left PARENTHESE_G PARENTHESE_D

/*nom_fonction, nom_fichier*/
%token <str> NAMEFUNC FILENAME
%token ERROR

%type <noeud> expr
%type <cc> frmt
%type <str> chaine

%%
    debut       : commandes ;
    commandes   : commandes cmd
                | EOL
				|;
    cmd         : FUNC NAMEFUNC DP expr EOL {
					
						Inst_expr * f = Expr_new($2, $4);
						Expr_insert(f);
					
                }
                | PRINT frmt NAMEFUNC chaine EOL {
                    Inst_expr * f = Expr_find($3);
                    if (f)
                        Expr_print(f,$2,$4);
                    else
						printf("La fonction %s n'existe pas\n\n", $3);
                }
				| EVAL NAMEFUNC REEL EOL {
                    Inst_expr * f = Expr_find($2);
                    
                    if (f)
                        printf("%s(%.2f) = %.2f\n", $2, $3, Expr_eval(f->expr, $3));
                    else
						printf("La fonction %s n'existe pas\n\n", $2);
                }
				| SIMPLIFY NAMEFUNC EOL {
					Inst_expr * f = Expr_find($2);
					if (f)
					{
						Expr_simplifier(f);
					}
                    else
                        printf("La fonction %s n'existe pas\n\n", $2);
				}
				| SIMPLIFY NAMEFUNC DP NAMEFUNC EOL {
					Inst_expr * f1 = Expr_find($2);
					if (!f1)
					{
						Inst_expr * f2 = Expr_find($4);
						if (f2)
						{
							f1 = Expr_copy(f2,$2);
							Expr_simplifier(f1);
							Expr_insert(f1);
						}
						else
							printf("La fonction %s n'existe pas\n\n", $4);
					}
                    else
                        printf("La fonction %s existe déjà\n\n", $2);
				}
				| PLOT listFname min_max EOL {
					// lance gnuplot
					if (system("gnuplot plot.p") < 0)
						printf("Erreur lors de la commande plot\n");
					firstFname = 1; // pour le prochain appel a Plot
				}
                ;
	min_max		: REEL REEL {
					int min = -100;
					int max = 100;
					
					if ($1 < $2)
					{
						min = $1;
						max = $2;
					}
					plot = fopen("plot.p", "w");
					fprintf(plot, "plot [%d:%d] ", min, max);
					// copie la liste de fonctions dans le fichier plot
					fprintf(plot, buffer);
					fprintf(plot, "\n");
					fclose(plot); // ferme le fichier plot
				}
				| {
					plot = fopen("plot.p", "w");
					fprintf(plot, "plot [-100:100] ");
					// copie la liste de fonctions dans le fichier plot
					fprintf(plot, buffer);
					fprintf(plot, "\n");
					fclose(plot); // ferme le fichier plot
				}
				;
	listFname	: listFname Fname ;
				|;
	Fname		: NAMEFUNC {
					// entre la liste des fonctions dans le buffer
					if (firstFname)
					{
						buffer[0] = '\0'; // vide le buffer
						firstFname = 0;
					}
					else
						safeStrCat(buffer, ", ");
					
                    Inst_expr * f = Expr_find($1);
                    if (f)
					{
						if (pa_toString(buffer, f->expr) != 0)
							printf("Erreur lors de la creation de la liste de fonctions\n");
					}
                    else
						printf("La fonction %s n'existe pas\n\n", $1);
				}
				;
    expr        : REEL {
					$$ = pa_newVal($1);
				}
                | XV {
					$$ = pa_newVar(); 
                }
                | expr PLUS expr {
                    $$ = pa_newPlus($1, $3);
				}
                | expr FOIS expr {
                    $$ = pa_newFois($1, $3);
                }
				| expr MOINS expr {
                    $$ = pa_newMoins($1, $3);
                }
				| PARENTHESE_G expr PARENTHESE_D {
                    $$ = $2;
                }
                | NAMEFUNC PARENTHESE_G expr PARENTHESE_D {
					Inst_expr * f = Expr_find($1);
					if (f == NULL)
					{
						char * dfname = strdup($1);
						int nb_der = 0;
						Inst_expr * primitive = NULL;
						// s'il s'agit d'une derivee, recherche la fonction correspondante
						while (!primitive && strstr(dfname, "'") != NULL)
						{
							dfname[strlen(dfname)-1] = '\0';
							primitive = Expr_find(dfname);
							nb_der++;
						}
						// si la fonction existe, on la derive 'nb_der' fois
						if (primitive)
						{
							Inst_expr * pf = primitive;
							Inst_expr * df = NULL;
							while (nb_der>0)
							{
								df = Expr_derive(pf);
								Expr_insert(df); 
								pf = df;
								df = NULL;
								nb_der--;
							}
							$$ = pa_newCall($1, $3);
						}
						else
						{
							printf("La fonction %s n'existe pas\n\n", $1);
							
						}
					}else
						$$ = pa_newCall($1, $3);
                }
                ;
    frmt  : FRM    {$$ = $1;}
           |       {$$ = 'e';}
                ;    
    chaine  : FILENAME      {$$ = $1;}
                |           {$$ = NULL;}
                ;
%%
	
#include "lex.yy.c"
	
	
int main(int argc, char **argv)
{
    int ret = yyparse();

    Expr_DestroyDB();

    return ret;
}
	
	

