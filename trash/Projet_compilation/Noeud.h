#ifndef NOEUD_H
#define NOEUD_H

#include <stdio.h>
#include <string.h>

#define BUFFER_MAX_SIZE 1000
#define PRECISION 1.0e-6


typedef enum _Tvar {
	VAL,
	VAR,
	ADD,
	MULT,
	SOUST,
	FCT
} Tvar;

typedef struct _Noeud Noeud;
struct _Noeud{
    Tvar type;
    float cte;
	char* str;
    Noeud* fg;
    Noeud* fd;
};

// return -1 si erreur, 0 sinon
int safeStrCat(char* dest, char* src);
/* fonction de construction d'expression logique. */
Noeud* pa_newVal(float v);
Noeud* pa_newVar();
Noeud* pa_newPlus(Noeud* fg, Noeud* fd);
Noeud* pa_newFois(Noeud* fg, Noeud* fd);
Noeud* pa_newMoins(Noeud* fg, Noeud* fd);
Noeud* pa_newCall(char* fName, Noeud* arg);
Noeud* pa_copy(Noeud* nc);
void pa_free(Noeud* n);

// DERIVEE
Noeud* pa_derive(Noeud* n);

float pa_eval(Noeud* expr, float arg);
void pa_print_e(FILE* out, Noeud* n);
void pa_print_a(FILE* out, Noeud* n);
//void cuiller_print_p(FILE* out, Noeud* n);

int pa_simplifier(Noeud*n);

int pa_toString(char* buffer, Noeud* n);

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

typedef struct _Inst_expr Inst_expr;
struct _Inst_expr{
	char * nom;
	Noeud * expr;
};

Inst_expr* Expr_new(char* nom, Noeud* expr);
Inst_expr* Expr_copy(Inst_expr *fc, char* nom);
void Expr_free(void* v);
int Expr_compare(const void* v1, const void* v2);
void Expr_insert(Inst_expr* key);
Inst_expr* Expr_find(char* nom);
void Expr_DestroyDB();
void Expr_printDB();
void Expr_print(Inst_expr* f, char format, char* fileName);
float Expr_eval(Noeud*,float);

void Expr_simplifier(Inst_expr* f);

Inst_expr* Expr_derive(Inst_expr* f);
char* nomDerive(char*nom);


#endif

