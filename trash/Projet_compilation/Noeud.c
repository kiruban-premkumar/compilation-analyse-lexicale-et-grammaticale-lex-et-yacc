#include "Noeud.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <search.h>

/* fonction de construction d'expression logique. */
Noeud* pa_newVal(float v)
{
	Noeud * ret = (Noeud*)malloc(sizeof(*ret));
	ret->type = VAL;
    ret->cte = v;
	ret->str = NULL;
    ret->fg = NULL;
    ret->fd = NULL;
	
	return ret;
}

Noeud* pa_newVar()
{
	Noeud * ret = (Noeud*)malloc(sizeof(*ret));
	ret->type = VAR;
    ret->cte = 0;
	ret->str = NULL;
    ret->fg = NULL;
    ret->fd = NULL;
	
	return ret;
}

Noeud* pa_newPlus(Noeud* fg, Noeud* fd)
{
	Noeud * ret = (Noeud*)malloc(sizeof(*ret));
	ret->type = ADD;
    ret->cte = 0;
	ret->str = NULL;
    ret->fg = fg;
    ret->fd = fd;
	
	return ret;
}



Noeud* pa_newMoins(Noeud* fg, Noeud* fd)
{
	Noeud * ret = (Noeud*)malloc(sizeof(*ret));
	ret->type = SOUST;
    ret->cte = 0;
	ret->str = NULL;
    ret->fg = fg;
    ret->fd = fd;
	
	return ret;
}



Noeud* pa_newFois(Noeud* fg, Noeud* fd)
{
	Noeud * ret = (Noeud*)malloc(sizeof(*ret));
	ret->type = MULT;
    ret->cte = 0;
	ret->str = NULL;
    ret->fg = fg;
    ret->fd = fd;
	
	return ret;
}

Noeud* pa_newCall(char* fName, Noeud* arg)
{
	Noeud * ret = (Noeud*)malloc(sizeof(*ret));
	ret->type = FCT;
    ret->cte = 0;
	ret->str = strdup(fName);
    ret->fg = arg;
    ret->fd = NULL;
	
	return ret;
}

Noeud* pa_copy(Noeud* nvn)
{
	if (nvn == NULL)
		return NULL;
	Noeud * ret = (Noeud*)malloc(sizeof(*ret));
	ret->type = nvn->type;
    ret->cte = nvn->cte;
	if (nvn->str != NULL)
		ret->str = strdup(nvn->str);
    ret->fg = pa_copy(nvn->fg);
    ret->fd = pa_copy(nvn->fd);
	
	return ret;
}

void pa_free(Noeud* n)
{
    if (!n)
        return;

	free(n->str);
    n->str = NULL;

    pa_free(n->fg);
    n->fg = NULL;

    pa_free(n->fd);
    n->fd = NULL;

    free(n);
}

// DERIVEE
Noeud* pa_derive(Noeud* n)
{
	if (!n)
		return NULL;
	
	char * fname = NULL;
    switch(n->type)
    {
        case VAL:
            return pa_newVal(0);
        case VAR:
            return pa_newVal(1);
        case ADD: case SOUST:
            return pa_newPlus(
				pa_derive(n->fg),
				pa_derive(n->fd)
			);
        case MULT:
            return pa_newPlus(
				pa_newFois(
					pa_derive(n->fg),
					n->fd
				),
				pa_newFois(
					n->fg,
					pa_derive(n->fd)
				)
			);
        case FCT:
			fname = (char*)malloc(sizeof(char)*strlen(n->str)+2); 
			strncat(fname, n->str, strlen(n->str));
			strcat(fname, "'");
            return pa_newFois(
				pa_newCall(
					fname,
					n->fg
				),
				pa_derive(n->fg)
			);
        default:
            return;
    }
}

int pa_simplifier(Noeud*n)
{
	int change = 0;
	switch(n->type)
	{
		case VAL:
			return 0;
		case VAR:
			return 0;
		case ADD:
			if (n->fg->type == VAL && n->fd->type == VAL)
			{
				n->type = VAL;
				n->cte = n->fg->cte + n->fd->cte;
				pa_free(n->fg);
				n->fg = NULL;
				pa_free(n->fd);
				n->fd = NULL;
				return 1;
			}else if (n->fg->type == MULT && n->fd->type == MULT)
			{
				Noeud*fg = n->fg;
				if ((fg->fg->type == VAL && fg->fd->type == VAR) || (fg->fg->type == VAR && fg->fd->type == VAL))
				{
					float val1 = fg->fg->type == VAL ? fg->fg->cte : fg->fd->cte;
					Noeud*fd = n->fd;
					if ((fd->fg->type == VAL && fd->fd->type == VAR) || (fd->fg->type == VAR && fd->fd->type == VAL))
					{
						float val2 = fd->fg->type == VAL ? fd->fg->cte : fd->fd->cte;
						
						n->type = MULT;
						fg->type = VAL;
						fg->cte = val1 + val2;
						pa_free(fg->fg);
						fg->fg = NULL;
						pa_free(fg->fd);
						fg->fd = NULL;
						
						fd->type = VAR;
						pa_free(fd->fg);
						fd->fg = NULL;
						pa_free(fd->fd);
						fd->fd = NULL;
						return 1;
					}
				}
			}
			change += pa_simplifier(n->fg);
			change += pa_simplifier(n->fd);
			return change;
		case MULT:
			if (n->fg->type == VAL && n->fd->type == VAL)
			{
				n->type = VAL;
				n->cte = n->fg->cte * n->fd->cte;
				pa_free(n->fg);
				n->fg = NULL;
				pa_free(n->fd);
				n->fd = NULL;
				return 1;
			}
			change += pa_simplifier(n->fg);
			change += pa_simplifier(n->fd);
			return change;
		case FCT:
			return pa_simplifier(n->fg);
		default:
			return 0;
	}
}


// PRINT
void pa_print_e(FILE* out, Noeud* n)
{
	if (!n)
		return;
	
	Noeud * fg = n->fg;
	Noeud * fd = n->fd;
    switch(n->type)
    {
        case VAL:
            fprintf(out,"%.2f",n->cte);
            return;
        case VAR:
            fprintf(out,"x");
            return;
        case ADD:
            pa_print_e(out, fg);
            fprintf(out,"+");
            pa_print_e(out, fd);
            return;
        case MULT:
			if (fg && fg->type == ADD)
				fprintf(out,"(");
			pa_print_e(out, fg);
			if (fg && fg->type == ADD)
				fprintf(out,")");
				
            fprintf(out,"*");
			
			if (fd && fd->type == ADD)
				fprintf(out,"(");
            pa_print_e(out, fd);
			if (fd && fd->type == ADD)
				fprintf(out,")");
            return;
        case FCT:
            fprintf(out,"%s(",n->str);
            pa_print_e(out, fg);
            fprintf(out,")");
            return;
        default:
            return;
    }
}

void pa_print_a(FILE*out, Noeud*n)
{
	switch (n->type)
	{
		case VAL:
			fprintf(out,"%d [label=\"%.2f\"];\n",n,n->cte);
			return;
		case VAR:
			fprintf(out,"%d [label=\"x\"];\n",n);
			return;
		case ADD:
			fprintf(out,"%d [label=\"+\"];\n",n);
			pa_print_a(out, n->fg);
			pa_print_a(out, n->fd);
			fprintf(out,"%d -> %d;\n",n,n->fg);
			fprintf(out,"%d -> %d;\n",n,n->fd);
			return;
		case MULT:
			fprintf(out,"%d [label=\"*\"];\n",n);
			pa_print_a(out, n->fg);
			pa_print_a(out, n->fd);
			fprintf(out,"%d -> %d;\n",n,n->fg);
			fprintf(out,"%d -> %d;\n",n,n->fd);
			return;
		case FCT:
			fprintf(out,"%d [label=\"%s\", shape=box];\n",n,n->str);
			pa_print_a(out,n->fg);
			fprintf(out,"%d -> %d [label=\"Parametres\", fontcolor=grey];\n",n,n->fg);
			return;
		default:
			return;
	}
}


int pa_toString(char* buffer, Noeud* n)
{
	if (!n)
		return;
	
	Noeud * fg = n->fg;
	Noeud * fd = n->fd;
	char valeur[20];
	int res = 0;
    switch(n->type)
    {
        case VAL:
			
            sprintf(valeur,"%.2f",n->cte);
			
            return safeStrCat(buffer, valeur);
			
        case VAR:
            return safeStrCat(buffer,"x");
			
        case ADD:
			return (
				pa_toString(buffer, fg) 
				+ safeStrCat(buffer,"+") 
				+ pa_toString(buffer, fd)
				);
			
        case MULT:
			if (fg && fg->type == ADD)
			{
				res += (
					safeStrCat(buffer,"(")
					+ pa_toString(buffer, fg)
					+ safeStrCat(buffer,")")
				);
			}
			else
				res += pa_toString(buffer, fg);
			
			res += safeStrCat(buffer,"*");
			
			if (fd && fd->type == ADD)
			{
				res += (
					safeStrCat(buffer,"(")
					+ pa_toString(buffer, fd)
					+ safeStrCat(buffer,")")
				);
			}
			else
				res += pa_toString(buffer, fd);
				
			return res;
			
        case FCT:
            return (
				safeStrCat(buffer, n->str)
				+ pa_toString(buffer, fg)
				+ safeStrCat(buffer,")")
			);
			
        default:
            return -1;
    }
}

/* Fonction de concatenation de strings */
// return -1 si erreur, 0 sinon
int safeStrCat(char* dest, char* src)
{
	if (!src)
		return 0;
	
	int size_left = BUFFER_MAX_SIZE - strlen(dest);
		
	// il reste de la place
	if (size_left > strlen(src))
	{
		strncat(dest, src, strlen(src));
		return 0;
	}
	
	// error
	return -1;
}

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

void* Expr_DB = 0;

Inst_expr* Expr_new(char* nom, Noeud* expr){
	Inst_expr* f = (Inst_expr*)malloc(sizeof(Inst_expr));
	f->nom = strdup(nom);
	f->expr = expr;
	return f;
}

Inst_expr* Expr_copy(Inst_expr *fc, char* nom)
{
	Inst_expr* f = (Inst_expr*)malloc(sizeof(Inst_expr));
	f->nom = strdup(nom);
	f->expr = pa_copy(fc->expr);
	return f;
}

void Expr_free(void* v){
	Inst_expr* f = (Inst_expr*)v;
	if(!f){
		return;
	}
	
	if(f->nom){
		free(f->nom);
		f->nom = 0;
	}

	if(f->expr){
		pa_free(f->expr);
		f->expr = 0;
	}

	free(f);
}

int Expr_compare(const void* v1, const void* v2){
	return strcmp(((Inst_expr*)v1)->nom, ((Inst_expr*)v2)->nom);
}

void print_tree(const void* node, VISIT order, int level){
	Inst_expr* f = *(Inst_expr**)node;
	if((postorder == order) || (leaf == order)){
		printf( "%d, %s, %s\n", level, f->nom, f->expr);
	}
}

void Expr_insert(Inst_expr* key){
	Inst_expr** ptr = 0;
	Inst_expr* res = 0;

	ptr = tsearch(key, &Expr_DB, Expr_compare);
	if(ptr){
		res = *ptr;
		if(res != key){
			
			fprintf(stderr, "La fonction %s existe deja\n", key->nom);
		}
	}
}

Inst_expr* Expr_find(char* nom){
	Inst_expr** ptr = 0;
	Inst_expr* res = 0;
	Inst_expr* key = 0;

	key = Expr_new(nom, NULL);
	ptr = tfind(key, &Expr_DB, Expr_compare);
	if(ptr){
		res = *ptr;
	}
	Expr_free(key);

	return res;
}

void Expr_DestroyDB()
{
	tdestroy(Expr_DB, Expr_free);
}


//--------------------------------
void Expr_printDB()
{
	twalk(Expr_DB, print_tree);
}

void Expr_print(Inst_expr* f, char format, char* fileName)
{
    FILE * file = stdout;
    if (fileName != NULL)
    {
        if ((file = fopen(fileName,"w"))==0)
            file = stdout;
    }
	
    switch (format)
    {
        
        case 'a':
            fprintf(file,"digraph %s{\n%d [label=\"%s\", shape=box];\n",f->nom,f,f->nom);
			pa_print_a(file, f->expr);
			fprintf(file,"%d -> %d;\n}\n",f,f->expr);
            break;
        default:
			fprintf(file,"%s: ",f->nom);
            pa_print_e(file, f->expr);
			fprintf(file,"\n");
			break;
    }
	
	if (file && file != stdout)
		fclose(file);
}

float Expr_eval(Noeud* expr, float arg)
{
    if (expr == NULL)
        return 0;
    switch(expr->type)
    {
        case VAL:
            return expr->cte;
        case VAR:
            return arg;
        case ADD:
            return Expr_eval(expr->fg, arg) + Expr_eval(expr->fd, arg);
        case MULT:
            return Expr_eval(expr->fg, arg) * Expr_eval(expr->fd, arg);
        case FCT:
            {
             Inst_expr * f = Expr_find(expr->str);
             
             if (f != NULL)
                return Expr_eval(f->expr, Expr_eval(expr->fg, arg));
			 fprintf(stderr,"Erreur: Fonction (%s) inexistante\n",expr->str);
             return -1; // erreur recuperation fonction
            }
        default:
            return 0;
    }
}

void Expr_simplifier(Inst_expr* f)
{
	int change = 0;
	do
	{
		change = pa_simplifier(f->expr);
	}while(change != 0);
}

Inst_expr* Expr_derive(Inst_expr* f)
{
	if (!f)
		return NULL;
	Expr_print(f, 'e', NULL);
	char * fname = nomDerive(f->nom);
	Expr_print(f, 'e', NULL);
	Noeud * derive = pa_copy(pa_derive(f->expr));
	Expr_print(f, 'e', NULL);
	pa_simplifier(derive);
	Expr_print(f, 'e', NULL);
	Inst_expr * ret = Expr_new(
		fname,
		derive
	);
	
	Expr_print(f, 'e', NULL);
	return ret;
}

char* nomDerive(char*nom)
{
	char * ret = (char*)malloc(sizeof(char)*strlen(nom)+2);
	int i;
	for(i=0; nom[i] != '\0'; i++)
		ret[i] = nom[i];
	ret[i] = '\'';
	ret[i+1] = '\0';
	return ret;
}
