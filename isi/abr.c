#include <stdlib.h>
#include <stdio.h> 
#include <string.h>
#include "isi.h"
#include "abr.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))

/**
 * \struct _Tnoeud
 * \brief Structure d'un noeud
 */
struct _Tnoeud
{
    char *contenu;
    Tnoeud *fils_gauche, *fils_droit;
    int hauteur;
};

/**
 * \brief Allocation d'un noeud
 * \param fils_gauche pointeur sur le noeud du fils gauche
 * \param contenu le contenu du noeud
 * \param fils_droit pointeur sur le noeud du fils droit
 * \return Tnoeud* le noeud alloué
 */
Tnoeud* abr_new(Tnoeud *fils_gauche, char *contenu, Tnoeud *fils_droit)
{
    Tnoeud* ret = (Tnoeud*)malloc(sizeof(*ret));
    ret->contenu = malloc(sizeof(char)*strlen(contenu));
    ret->contenu = strdup(contenu);
    ret->fils_gauche = fils_gauche;
    ret->fils_droit = fils_droit;
    abr_calculerHauteur(&ret);
    return ret;
}

/**
 * \brief Free d'un arbre récursivement
 * \param *a le pointeur de l'arbre à libérer
 */
void abr_free(Tnoeud *a)
{
    if(a!=0)
    {
        abr_free(a->fils_gauche);
        abr_free(a->fils_droit);
        free(a);
    }
}

/**
 * \brief Hauteur d'un arbre
 * \param *a le pointeur de l'arbre dont on veut connaitre l'hauteur
 * \return int l'hauteur de l'arbre
 */
int abr_hauteur(Tnoeud *a)
{
    return (a == 0)?-1:a->hauteur;
}

/**
 * \brief Calcul et modification de l'hauteur d'un arbre par rapport à ses fils
 * \param **a le pointeur de pointeur de l'arbre dont on veut modifier l'hauteur
 */
void abr_calculerHauteur(Tnoeud **a)
{
    (*a)->hauteur = 1 + MAX(abr_hauteur((*a)->fils_gauche),abr_hauteur((*a)->fils_droit));
}

/**
 * \brief Rotation gauche d'un arbre
 * \param **a le pointeur de pointeur de l'arbre dont on veut faire une rotation gauche
 */
static void abr_rotationGauche(Tnoeud **a)
{
    Tnoeud *b = (*a)->fils_droit;
    Tnoeud *c = abr_new((*a)->fils_gauche,(*a)->contenu,b->fils_gauche);
    *a = abr_new(c,b->contenu,b->fils_droit);
}

/**
 * \brief Rotation droite d'un arbre
 * \param **a le pointeur de pointeur de l'arbre dont on veut faire une rotation droite
 */
static void abr_rotationDroite(Tnoeud **a)
{
    Tnoeud *b = (*a)->fils_gauche;
    Tnoeud *c = abr_new(b->fils_droit,(*a)->contenu,(*a)->fils_droit);
    *a = abr_new(b->fils_gauche,b->contenu,c);
}

/**
 * \brief Ajout d'un noeud
 * \param **a l'arbre à laquel on veut ajouter le noeud
 * \param contenu le contenu que l'on veut ajouter au noeud
 * \param e option d'équilibrage
 * \param g option de fils unique, du plus grand au plus petit
 * \param d option de fils unique, du plus petit au plus grand
 */
void abr_ajouter(Tnoeud **a, char *contenu, int e, int g, int d)
{
    if(*a==0)
    {
        (*a)=abr_new(0,contenu,0);
        return;
    }

    if(g==0 && d==0)
    {
        if(strcmp(contenu,(*a)->contenu) < 0)
            abr_ajouter(&(*a)->fils_gauche, contenu, e,g,d);
        else
            abr_ajouter(&(*a)->fils_droit, contenu, e,g,d);

        if(e == 1)
            abr_equilibrer(&(*a));
    }
    else
    {
        if((strcmp((*a)->contenu,contenu) > 0 && g == 0) || (strcmp((*a)->contenu,contenu) < 0 && g == 1))
        {
            Tnoeud *b = *a, *c = NULL;
            abr_ajouter(&c, contenu,e,g,d);
            *a = c;
            if(g==1)
                c->fils_gauche = b;
            else
                c->fils_droit = b;
        }
        else
        {
            if(g==1)
                abr_ajouter(&(*a)->fils_gauche, contenu,e,g,d);
            else
                abr_ajouter(&(*a)->fils_droit, contenu,e,g,d);
        }
    }
}

/**
 * \brief Equilibrage d'un noeud
 * \param **a l'arbre que l'on veut équilibrer
 */
void abr_equilibrer(Tnoeud **a)
{
    abr_calculerHauteur(&(*a));
    if(abr_hauteur((*a)->fils_gauche) - abr_hauteur((*a)->fils_droit) == 2)
    {
        if(abr_hauteur(((*a)->fils_gauche)->fils_gauche) < abr_hauteur(((*a)->fils_gauche)->fils_droit))
            abr_rotationGauche(&(*a)->fils_gauche);
        abr_rotationDroite(&(*a));
    }

    if(abr_hauteur((*a)->fils_gauche) - abr_hauteur((*a)->fils_droit) == -2)
    {
        if(abr_hauteur(((*a)->fils_droit)->fils_droit) < abr_hauteur(((*a)->fils_droit)->fils_gauche))
            abr_rotationDroite(&(*a)->fils_droit);
        abr_rotationGauche(&(*a));
    }
}

/**
 * \brief Affichage des noeuds de l'arbre
 * \param *f fichier sur lequel on écrit l'arbre au format dot
 * \param *a l'arbre à afficher
 */
void abr_afficher(FILE *f, Tnoeud *a)
{
    if(a!=0)
    {
        if(a->fils_gauche != 0)
            fprintf(f,"     %s -> %s [label=g]\n", a->contenu,(a->fils_gauche)->contenu);
        if(a->fils_droit != 0)
            fprintf(f,"     %s -> %s [label=d]\n", a->contenu,(a->fils_droit)->contenu);
        abr_afficher(f,a->fils_gauche);
        abr_afficher(f,a->fils_droit);
    }
}

/**
 * \brief Ecriture de la structure du fichier dot et exécution des commandes systèmes pour générer le pdf
 * \param *a l'arbre à visualiser
 * \param x option de génération du pdf
 */
void abr_dot(Tnoeud *a, int x)
{
    FILE *f;

    if(x == 1){
        f = fopen("arbre.dot","w");
        if(f == NULL)
            fprintf(stderr, "abr_dot : erreur d'ouverture du fichier.\n");
    } else {
        f = stdout;
    }

    fprintf(f,"digraph G {\n");
    abr_afficher(f,a);
    fprintf(f, "}\n");
    fclose(f);
    
    if(x == 1){
        system("dot -Tpdf arbre.dot -o arbre.pdf");
        system("xpdf arbre.pdf");
    }
}