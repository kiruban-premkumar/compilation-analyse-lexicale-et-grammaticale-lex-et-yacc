%{
#include <stdio.h>
#include "pico.h"
%}

%union {
   int val;
   char * str;
   Texpression* expr;
   TlisteOperande* liste_operande;
   ToperandeType* operande;
   Tais* ais;
}

// primitives
%token TK_ORG
%token TK_LONG
%token TK_WORD
%token TK_BYTE
%token TK_STRING

// types
%token<val> TK_ENTIER
%token<str> TK_STR
%token<str> TK_IDENT

// operations
%token TK_ADD
%token TK_SUB
%token TK_MUL
%token TK_MOV
%token TK_JMP
%token TK_IF
%token TK_ELSE
%token TK_CALL
%token TK_RET
%token TK_IN
%token TK_OUT
%token TK_STOP
%token TK_LABEL
%token<operande> TK_REG
%token TK_PLUSPLUS
%token<ais> TK_AIS

// expression
%token TK_SHIFT
%token TK_MULTIPLICATION
%token TK_PLUS
%token TK_MOINS


// operateurs de comparaisons
%token<operateur> TK_EGAL
%token<operateur> TK_DIFFERENT
%token<operateur> TK_INFERIEUR
%token<operateur> TK_SUPERIEUR
%token<operateur> TK_INFERIEUR_EGAL
%token<operateur> TK_SUPERIEUR_EGAL

%token TK_ERREUR

%type<operande> operande
%type<expr> expression
%type<liste_operande> suite_operande
%type<ais> ais
%type<primitive> primitives
%type<instruction> instructions
%type<operateur> operateur_de_comparaison

%left TK_SHIFT
%left TK_MULTIPLICATION
%left TK_MOINS
%left TK_PLUS

%%

script
	:	suite_commandes;

suite_commandes
	:	suite_commandes commande 
	| 	/* RIEN */;

commande
	: TK_LABEL
	| primitives 
	| instructions
;

primitives
	: TK_ORG TK_ENTIER { $$=primitive_new_org($2); }
    | TK_LONG expression { $$=primitive_new_long($2); }
    | TK_WORD expression { $$=primitive_new_word($2); }
    | TK_BYTE expression { $$=primitive_new_byte($2); }
    | TK_STRING TK_STR { $$=primitive_new_string($2); }
;

instructions
	: TK_ADD operande suite_operande ais {$$=instruction_new_add($2,$3,$4);}
	| TK_SUB operande suite_operande ais {$$=instruction_new_sub($2,$3,$4);}
	| TK_MUL operande suite_operande ais {$$=instruction_new_mul($2,$3,$4);}
	| TK_MOV operande operande ais {$$=instruction_new_mov($2,$3,$4);}
	| TK_JMP expression {$$=instruction_new_jmp($2);}
	| TK_IF operande operateur_de_comparaison operande expression {$$=instruction_new_if($2,$3,$4,$5);}
	| TK_IF operande operateur_de_comparaison operande expression  
                TK_ELSE expression {$$=instruction_new_ifelse($2,$3,$4,$5,$7);}
	| TK_CALL expression ais {$$=instruction_new_call($2,$3);}
	| TK_RET {$$=instruction_new_ret();}
	| TK_IN operande ais {$$=instruction_new_in($2,$3);}
	| TK_OUT operande ais {$$=instruction_new_out($2,$3);}
	| TK_STOP {$$=instruction_new_stop();}
;

operande
	: '$' expression  { $$= operande_new_IMM($2); }
	| expression { $$=operande_new_DM($1); }
	| '(' expression ')' {$$=operande_new_INM($2);}
	| TK_REG {$$=operande_new_DR($1);}
	| '(' TK_REG ')' { $$=operande_new_IR($2); }
	| TK_PLUSPLUS '(' TK_REG ')' {$$=operande_new_PPIR($3);}
	| '(' TK_REG ')' TK_PLUSPLUS {$$=operande_new_IRPP($2);}
;

suite_operande
	: operande suite_operande { $$=$1; operande_add_at_tail($1,$2); }
	| /* RIEN */   { $$=0; }
;

expression
	: TK_IDENT { $$=expr_new_ident($1); }
	| TK_ENTIER { $$=expr_new_entier($1); }
	| '(' expression ')' { $$=expr_new_parenthese($2); }
	| expression TK_PLUS expression { $$=expr_new_plus($1,$3); }
	| expression TK_MOINS expression { $$=expr_new_moins($1,$3); }
	| expression TK_MULTIPLICATION expression { $$=expr_new_multiplication($1,$3); }
	| expression TK_SHIFT expression { $$=expr_new_shift($1,$3); }
	
;

operateur_de_comparaison
	: TK_EGAL {}
	| TK_DIFFERENT
	| TK_INFERIEUR
	| TK_SUPERIEUR
	| TK_INFERIEUR_EGAL
	| TK_SUPERIEUR_EGAL
;

ais
	: TK_AIS expression { $$ = ais_new(1,$2); }
	| /* RIEN */ {$$= ais_new(0);}
;

%%

#include "lex.yy.c"


int yyerror(char *s) 
{
 
  printf("%d:  erreur %s (near symbol %s)\n",yylineno,s,yytext);
  exit(1);
}

int main(void) {
  yyparse();
  printf("correct\n");
  return 0;
}