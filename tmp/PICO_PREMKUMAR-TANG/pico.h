#ifndef PICO_H
#define PICO_H

#include <stdio.h>
#include <string.h>

typedef enum Toperation {
	ADD, SUB, MUL, MOV, JMP, JE, JL, JLE, CALL, RET, IN, OUT, STOP
} Toperation;

typedef enum ToperandeType { 
	EA_IMM, EA_DM, EA_INM, EA_DR, EA_IR, EA_IRPP, EA_PPIR 
} ToperandeType;

typedef struct _Texpression Texpression;
struct _Texpression {
	Texpression* fd;
	Texpression* fg;
	int parenthese;
  	Toperation type;
  	int value;    // une constante
  	char* ident; // le label

};

typedef struct Tais {
    int bool_ais; // 0 ou 1
    Texpression* addr;
 } Tais;

typedef struct _TlisteOperande TlisteOperande;
struct _TlisteOperande {
    TlisteOperande *nxt;
    ToperandeType type;
    Texpression* addr;
    int reg;
};
 
typedef struct Tinstruction
{
	Toperation op;
	int taille_operandes; // 1, 2 ou 4
	int taille_instruction;  // en byte
	TlisteOperande operandes;
	Texpression *adresse_branchement;
	Texpression *adresse_branchement2;
} Tinstruction;

TlisteOperande* operande_malloc();
TlisteOperande* operande_new_INM(Texpression *expr);
TlisteOperande* operande_new_IMM(Texpression* expr);
TlisteOperande* operande_new_DM(Texpression* expr);
TlisteOperande* operande_new_DR(int val);
TlisteOperande* operande_new_IR(int val);
TlisteOperande* operande_new_IRPP(int val);
TlisteOperande* operande_new_PPIR(int val);
TlisteOperande* operande_add_at_tail(TlisteOperande* operande,TlisteOperande* list);

Tais* ais_new(int bool_ais,Texpression* expr);

Texpression* expr_new_ident(char* ident);
Texpression* expr_new_entier(int entier);
Texpression* expr_new_parenthese(Texpression* expr);
Texpression* expr_new_plus(Texpression* expr1,Texpression* expr2);
Texpression* expr_new_moins(Texpression* expr1,Texpression* expr2);
Texpression* expr_new_multiplication(Texpression* expr1,Texpression* expr2);
Texpression* expr_new_shift(Texpression* expr1,Texpression* expr2);

#endif