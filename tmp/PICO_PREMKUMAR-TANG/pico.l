%{
#include<stdio.h>
%}

%option noyywrap

%%

#.* 		{ /* RIEN */ }

".org"		{return TK_ORG;}
".long"		{return TK_LONG;}
".word"		{return TK_WORD;}
".byte"		{return TK_BYTE;}
".string"	{return TK_STRING;}
	
"+"	{ return TK_PLUS; }
"*"  	{ return TK_MULTIPLICATION; }
"-"	{ return TK_MOINS; }

"movl"	{ yylval.val=4; return TK_MOV; }
"movw"	{ yylval.val=2; return TK_MOV; }
"movb"	{ yylval.val=1; return TK_MOV; }

"jmp"	{ return TK_JMP; }

"else"	{ return TK_ELSE; }
"call"	{ return TK_CALL; }
"ret"	{ return TK_RET; }

"inl"	{ yylval.val=4; return TK_IN; }
"inw"	{ yylval.val=2; return TK_IN; }
"inb"	{ yylval.val=1; return TK_IN; }

"addl"   { yylval.val=4; return TK_ADD; }
"addw"   { yylval.val=2; return TK_ADD; }
"addb"   { yylval.val=1; return TK_ADD; }

"subl"   { yylval.val=4; return TK_SUB; }
"subw"   { yylval.val=2; return TK_SUB; }
"subb"   { yylval.val=1; return TK_SUB; }

"mull"   { yylval.val=4; return TK_MUL; }
"mulw"   { yylval.val=2; return TK_MUL; }
"mulb"   { yylval.val=1; return TK_MUL; }

"ifl"   { yylval.val=4; return TK_IF; }
"ifw"   { yylval.val=2; return TK_IF; }
"ifb"   { yylval.val=1; return TK_IF; }

"outl"   { yylval.val=4; return TK_OUT; }
"outw"   { yylval.val=2; return TK_OUT; }
"outb"   { yylval.val=1; return TK_OUT; }

"stop"	{ return TK_STOP; }		
"%"[0-9]	{ yylval.val = atoi(yytext); return TK_REG; }
"++"	{ return TK_PLUSPLUS; }
"@"	{ return TK_AIS; }

"<<"	{ return TK_SHIFT; }
	
"="	{ return TK_EGAL; }
"!"  	{ return TK_DIFFERENT; }
"<"  	{ return TK_INFERIEUR; }
"<="|"=<"  { return TK_INFERIEUR_EGAL; }
">"  	{ return TK_SUPERIEUR; }
">="|"=>" { return TK_SUPERIEUR_EGAL; }


[0-9]+  { yylval.val=atoi(yytext); return TK_ENTIER; }
\"[^"\n]*\"   { yylval.str=strdup(yytext); return TK_STR; }
[A-Za-z_][0-9A-Za-z_]*   { yylval.str=strdup(yytext); return TK_IDENT; }
[A-Za-z_][0-9A-Za-z_]*":"   { yylval.str=strdup(yytext); return TK_LABEL; }

[()$] { return *yytext; }
[ \t] 		;
\n 		{ yylineno++; }

. 	{ return TK_ERREUR; }


%%

