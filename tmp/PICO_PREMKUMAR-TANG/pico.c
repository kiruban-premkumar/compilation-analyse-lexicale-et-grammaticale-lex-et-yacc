#include "pico.h"
#include <stdio.h>
#include <stdlib.h>


/**
* LES OPERANDES
**/

TlisteOperande* operande_malloc()
{
	TlisteOperande* ret = (TlisteOperande*) malloc(sizeof(*ret));
	ret->nxt = NULL;
	ret->type = 0;
	ret->addr = NULL;
	ret->reg = 0;
	return ret;
}

TlisteOperande* operande_new_INM(Texpression *expr)
{
	TlisteOperande* ret = operande_malloc();
	ret->type = EA_INM;
	ret->addr = expr;
	return ret;
}

TlisteOperande* operande_new_IMM(Texpression* expr)
{	
	TlisteOperande* ret = operande_malloc();
	ret->type = EA_IMM;
	ret->addr = expr;
	return ret;
}

TlisteOperande* operande_new_DM(Texpression* expr)
{	
	TlisteOperande* ret = operande_malloc();
	ret->type = EA_DM;
	ret->addr = expr;
	return ret;
}

TlisteOperande* operande_new_DR(int val)
{	
	TlisteOperande* ret = operande_malloc();
	ret->type = EA_DR;
	ret->reg = val;
	return ret;
}

TlisteOperande* operande_new_IR(int val)
{	
	TlisteOperande* ret = operande_malloc();
	ret->type = EA_IR;
	ret->reg = val;
	return ret;
}

TlisteOperande* operande_new_IRPP(int val)
{	
	TlisteOperande* ret = operande_malloc();
	ret->type = EA_IRPP;
	ret->reg = val;
	return ret;
}

TlisteOperande* operande_new_PPIR(int val)
{	
	TlisteOperande* ret = operande_malloc();
	ret->type = EA_PPIR;
	ret->reg = val;
	return ret;
}

TlisteOperande* operande_add_at_tail(TlisteOperande* operande,TlisteOperande* list)
{
	/* On ajoute en fin, donc aucun élément ne va suivre */
    operande->nxt = NULL;

    if(list == NULL)
    {
        /* Si la liste est vide il suffit de renvoyer l'operande */
        return operande;
    }
    else
    {
        /* Sinon, on parcourt la liste à l'aide d'un pointeur temporaire et on
        indique que le dernier élément de la liste est relié au nouvel élément */
        TlisteOperande* temp=list;
        while(temp->nxt != NULL)
        {
            temp = temp->nxt;
        }
        temp->nxt = operande;
        return list;
    }
}

/**
* AIS
**/

Tais* ais_new(int bool_ais,Texpression* expr)
{
	Tais* ret = (Tais*) malloc(sizeof(*ret));
	ret->bool_ais = bool_ais;
	ret->addr = expr;
	return ret;
}


/**
* LES EXPRESSIONS
**/

Texpression* expr_new_ident(char* ident)
{
	Texpression* ret = (Texpression*)malloc(sizeof(*ret));
	ret->fd = NULL;
	ret->fg = NULL;
	ret->parenthese = 0;
	ret->type = 0;
	ret->value = 0;
	ret->ident = malloc(sizeof(char)*strlen(ident));
	ret->ident = strdup(ident);
	return ret;
}

Texpression* expr_new_entier(int entier)
{
	Texpression* ret = (Texpression*)malloc(sizeof(*ret));
	ret->fd = NULL;
	ret->fg = NULL;
	ret->parenthese = 0;
	ret->type = 0;
	ret->value = entier;
	ret->ident = NULL;
	return ret;
}

Texpression* expr_new_parenthese(Texpression* expr)
{
	Texpression* ret = (Texpression*)malloc(sizeof(*ret));
	ret = expr;
	ret->parenthese = 1;
	return ret;
}

Texpression* expr_new_plus(Texpression* expr1,Texpression* expr2)
{
	Texpression* ret = (Texpression*)malloc(sizeof(*ret));
	ret->fd = expr1;
	ret->fg = expr2;
	ret->parenthese = 0;
	ret->type = ADD;
	ret->value = 0;
	ret->ident = NULL;
	return ret;
}

Texpression* expr_new_moins(Texpression* expr1,Texpression* expr2)
{
	Texpression* ret = (Texpression*)malloc(sizeof(*ret));
	ret->fd = expr1;
	ret->fg = expr2;
	ret->parenthese = 0;
	ret->type = SUB;
	ret->value = 0;
	ret->ident = NULL;
	return ret;
}

Texpression* expr_new_multiplication(Texpression* expr1,Texpression* expr2)
{
	Texpression* ret = (Texpression*)malloc(sizeof(*ret));
	ret->fd = expr1;
	ret->fg = expr2;
	ret->parenthese = 0;
	ret->type = MUL;
	ret->value = 0;
	ret->ident = NULL;
	return ret;
}

Texpression* expr_new_shift(Texpression* expr1,Texpression* expr2)
{
	Texpression* ret = (Texpression*)malloc(sizeof(*ret));
	ret->fd = expr1;
	ret->fg = expr2;
	ret->parenthese = 0;
	ret->type = MOV;
	ret->value = 0;
	ret->ident = NULL;
	return ret;
}
