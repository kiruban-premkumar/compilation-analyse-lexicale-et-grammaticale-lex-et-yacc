
%option noyywrap

%%

".org" { return TK_ORG; }
".long" { return TK_LONG; }
".word" { return TK_WORD; }
".byte" { return TK_BYTE; }
".string" { return TK_STR; }

add[wbl] { 
            yylval.ope.ope= ADD; 
            yylval.ope.taille = yytext[3] == 'b' ? 1
                              : yytext[3] == 'w' ? 2 
                              : yytext[3] == 'l' ? 4 : 0; return TK_ASM; }
sub[wbl] { 
            yylval.ope.ope= SUB; 
            yylval.ope.taille = yytext[3] == 'b' ? 1
                              : yytext[3] == 'w' ? 2 
                              : yytext[3] == 'l' ? 4 : 0; return TK_ASM; }
mul[wbl] {  
            yylval.ope.ope= MUL; 
            yylval.ope.taille = yytext[3] == 'b' ? 1
                              : yytext[3] == 'w' ? 2 
                              : yytext[3] == 'l' ? 4 : 0; return TK_ASM; }
if[wbl] { 
            yylval.ope.taille = yytext[3] == 'b' ? 1
                              : yytext[3] == 'w' ? 2 
                              : yytext[3] == 'l' ? 4 : 0; return TK_IF; }
mov[wbl] { 
            yylval.ope.taille = yytext[3] == 'b' ? 1
                              : yytext[3] == 'w' ? 2 
                              : yytext[3] == 'l' ? 4 : 0; return TK_MOV; }
in[wbl] {
            yylval.ope.taille = yytext[3] == 'b' ? 1
                              : yytext[3] == 'w' ? 2 
                              : yytext[3] == 'l' ? 4 : 0; return TK_IN; }
out[wbl] { 
            yylval.ope.taille = yytext[3] == 'b' ? 1
                              : yytext[3] == 'w' ? 2 
	                      : yytext[3] == 'l' ? 4 : 0; return TK_OUT; }

"jmp" { return TK_JMP; }
"stop" { return TK_STOP; } 
"call" { return TK_CALL; }
"ret" {return TK_RET;}
"else" {return TK_ELSE;}
"$" {return TK_IMM;}
"%" {return TK_REG;}
"++" {return TK_PP;}
"+" { return TK_ARITH_PLUS; }
"-" { return TK_ARITH_MOINS; }
"*" { return TK_ARITH_MULT; }
"<<"  { return TK_ARITH_SHIFT; }  
[()]    { return *yytext; }
"@"    { return TK_AIS; }
["=""==""!=""<"">""<="">=""=<""=>"] { return TK_CMP_OPERATEUR; }
[a-zA-Z][0-9a-zA-Z]*: { return TK_LABEL; }
[a-zA-Z][0-9a-zA-Z]* { return TK_IDENT; }
[0-9]+ {yylval.val = atoi(yytext); return TK_INT;}
\"[^"\n]*\" { yytext[yyleng-1]=0; yylval.str = strdup(yytext +1); return TK_CHAINE;}
#.*;
[ \t\n];
