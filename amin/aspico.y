%{

#include "aspico.h"

  typedef struct t_operation_taille{
    t_operation ope;
    int taille; //0 rien 1 b 2 w 4 l
  }t_operation_taille;

  typedef struct t_ais{
    int bool_ais; // 0 si pas d'ais 1 si oui
    int adresse;
  }

%}

%union {
  int val;
  char * str;
  t_operation_taille ope;
}

%token<ilabel> TK_LABEL
%token<iprimitive> TK_ORG TK_LONG TK_WORD TK_BYTE TK_STR
%token<iprimitive> TK_CHAINE
%token<val> TK_INT
%token<str> TK_IDENT
%token<expr> TK_ARITH_PLUS TK_ARITH_MOINS TK_ARITH_MULT TK_ARITH_SHIFT
%token<oprde> TK_REG
%token<iasm> TK_ASM TK_JMP TK_IF TK_MOV TK_STOP TK_IN TK_OUT TK_CALL TK_RET TK_ELSE
%token TK_IMM TK_PP
%token<iasm> TK_CMP_OPERATEUR 
%token<ais> TK_AIS

%type<ilabel> i_label
%type<iasm> i_asm
%type<iprimitive> i_primitive
%type<oprde> operande
%type<expr> expression
%type<tab_oprde> liste_operande
%type<ais> ais

%left TK_ARITH_SHIFT 
%left TK_ARITH_MULT
%left TK_ARITH_MOINS
%left TK_ARITH_PLUS
%%

programme : suite_instruction;

suite_instruction
 : suite_instruction instruction 
 | instruction  
 ;

instruction
: i_label           
| i_primitive      
| i_asm             
;

i_label
: TK_LABEL              { $$ = new_label($1); }
;

i_primitive 
: TK_ORG expression     { $$ = new_primitive_org($2); }
 | TK_LONG expression   { $$ = new_primitive(4, $2); }
 | TK_WORD expression   { $$ = new_primitive(2, $2); }
 | TK_BYTE expression   { $$ = new_primitive(1, $2); }
 | TK_STR TK_CHAINE     { $$ = new_primitive_str($2.str); }
 ;

i_asm
: TK_ASM operande liste_operande ais       { $$ = operation_new_asm($1.ope, $1.taille, $2, $3 ,$4); }
 | TK_JMP expression                       { $$ = operation_new_jmp(JMP, $2); }
 | TK_IF operande TK_CMP_OPERATEUR operande expression    { $$ = operation_new_if($1.taille, $2, $3, $4, $5 ); }
 | TK_IF operande TK_CMP_OPERATEUR operande expression TK_ELSE expression   { $$ = operation_new_ifelse($1.taille, $2, $3, $4, $5, $7); }
 | TK_MOV operande operande ais              { $$ = operation_new_mv(MOV, $1.taille, $2, $3, $4); }
 | TK_STOP ais                               { $$ = operation_new_stop(STOP, $2); }
 | TK_IN operande ais                        { $$ = operation_new_in(IN, $1.taille, $2, $3); }
 | TK_OUT operande ais                       { $$ = operation_new_out(OUT, $1.taille, $2, $3); }
 | TK_CALL expression ais                    { $$ = operation_new_call(CALL, $2, $3); }
 | TK_RET ais                                { $$ = operation_new_ret(RET, $2); }
;

liste_operande
: liste_operande operande   { $$= tab_operande_new_oprde($1, $2); }
 | operande                 { $$ = tab_operande_new(); $$ = tab_operande_new_oprde($$, $1); }
;

operande 
: expression               { $$ = operande_new_withExpr($1->parenthesee?EA_IM:EA_DM, $1); }
 | TK_IMM expression       { $$ = operande_new_withExpr(EA_IMM, $2); }
 | TK_REG                  { $$ = operande_new_withReg(EA_DR, $1); }
 | '(' TK_REG ')'          { $$ = operande_new_withReg(EA_IR, $2); $$->parenthesee=1; }
 | TK_PP  '(' TK_REG ')'   { $$ = operande_new_withReg(EA_PPIR, $3); $$->parenthesee=1; }
 | '(' TK_REG ')' TK_PP    { $$ = operande_new_withReg(EA_IRPP, $2); $$->parenthesee=1; }
;

expression
: TK_INT                                { $$ = expr_new_cons(OE_CONST, $1.val); }
 | TK_IDENT                             { $$ = expr_new_ident(OE_IDENT, $1); }
 | expression TK_ARITH_PLUS expression  { $$ = expr_new_arith($1, OE_ADD, $2); }
 | expression TK_ARITH_MOINS expression { $$ = expr_new_arith($1, OE_SUB, $2); }
 | expression TK_ARITH_MULT expression  { $$ = expr_new_arith($1, OE_MULT, $2); }
 | expression TK_ARITH_SHIFT expression { $$ = expr_new_arith($1, OE_SHIFT, $2); }
 | '(' expression ')'                   { $$ = operande_new_wizExpr(EA_IM, $2); $$->parenthesee=1;}
 ;

ais 
: TK_AIS expression    { $$ = new_ais(1, $2); }                 
 |                     { $$ = new_ais(0); }
 ;

%%

#include "lex.yy.c"

 int yyerror(char * str){
   fprintf(stderr, "Incorrect\n");   
   exit(1);
 }

int main(int argc, char ** argv){
  yyparse();
  return 0;
}
