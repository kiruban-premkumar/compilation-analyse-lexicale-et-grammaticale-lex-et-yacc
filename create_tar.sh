#!/bin/bash

mkdir tmp
cd tmp
mkdir PICO_PREMKUMAR-TANG
cd PICO_PREMKUMAR-TANG
cp ../../pico.{c,h,l,y} .
cp ../../Makefile .
cp ../../README.txt .
cd ..
tar czvf PICO_PREMKUMAR-TANG.tar.gz PICO_PREMKUMAR-TANG/
cp PICO_PREMKUMAR-TANG.tar.gz ..
cd ..
#rm -rf tmp
