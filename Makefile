all: pico

pico:
	flex pico.l
	bison pico.y
	cc -o pico pico.c pico.tab.c -lfl

clean:
	rm pico.tab.c
	rm lex.yy.c

clean-all: clean
	rm pico